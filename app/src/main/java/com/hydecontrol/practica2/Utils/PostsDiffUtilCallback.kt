package com.hydecontrol.practica2.Utils

import android.support.v7.util.DiffUtil
import com.hydecontrol.practica2.model.DataAlbums
import com.hydecontrol.practica2.model.DataPosts
import java.security.PrivateKey

class PostsDiffUtilCallback(private val oldList: List<DataPosts>, private val newList: List<DataPosts>) : DiffUtil.Callback() {

    override fun getOldListSize() = oldList.size

    override fun getNewListSize() = newList.size

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int) = oldList[oldItemPosition].id == newList[newItemPosition].id

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int) = true
}
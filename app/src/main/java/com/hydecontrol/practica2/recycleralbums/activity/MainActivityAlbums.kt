package com.hydecontrol.practica2.recycleralbums.activity

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import com.hydecontrol.practica2.R
import com.hydecontrol.practica2.model.DataAlbums
import com.hydecontrol.practica2.recycleralbums.adapters.AlbumAdapter
import com.hydecontrol.practica2.retrofit.ApiClient
import kotlinx.android.synthetic.main.activity_main_albums.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class MainActivityAlbums : AppCompatActivity() {

    var albumList = ArrayList<DataAlbums>()
    lateinit var recyclerView: RecyclerView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main_albums)
        recyclerView = recycler_view_albums

        recycler_view_albums.adapter = AlbumAdapter(albumList, this)
        recycler_view_albums.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)

        getData2()
    }

    private fun getData2() {
        val call: Call<List<DataAlbums>> = ApiClient.getClient.getAlbums()
        call.enqueue(object : Callback<List<DataAlbums>> {

            override fun onResponse(call: Call<List<DataAlbums>>?, response: Response<List<DataAlbums>>?) {
                albumList.addAll(response!!.body()!!)
                recyclerView.adapter?.notifyDataSetChanged()
            }

            override fun onFailure(call: Call<List<DataAlbums>>?, t: Throwable?) {
            }
        })
    }
}

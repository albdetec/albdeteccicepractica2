package com.hydecontrol.practica2.recycleralbums.activity

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import com.hydecontrol.practica2.R
import com.hydecontrol.practica2.recycleralbums.adapters.AlbumDetailAdapter
import com.hydecontrol.practica2.model.DataPhotos
import com.hydecontrol.practica2.retrofit.ApiClient
import kotlinx.android.synthetic.main.detail_album.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class AlbumDetails : AppCompatActivity() {


    var photoList = ArrayList<DataPhotos>()
    lateinit var recyclerView: RecyclerView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.detail_album)
        recyclerView = album_detail_recycler

        album_detail_recycler.adapter =
                AlbumDetailAdapter(photoList, this)
        album_detail_recycler.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)

        getData1()
    }

    private fun getData1() {
        val name = intent.getStringExtra("name")
        val call: Call<List<DataPhotos>> = ApiClient.getClient.getPhotos(name)
        call.enqueue(object : Callback<List<DataPhotos>> {

            override fun onResponse(call: Call<List<DataPhotos>>?, response: Response<List<DataPhotos>>?) {
                photoList.addAll(response!!.body()!!)
                recyclerView.adapter!!.notifyDataSetChanged()
            }

            override fun onFailure(call: Call<List<DataPhotos>>?, t: Throwable?) {
            }
        })
    }

}
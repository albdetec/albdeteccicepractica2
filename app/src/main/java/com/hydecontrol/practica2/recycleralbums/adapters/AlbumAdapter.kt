package com.hydecontrol.practica2.recycleralbums.adapters

import android.content.Context
import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.hydecontrol.practica2.R
import com.hydecontrol.practica2.recycleralbums.activity.AlbumDetails
import com.hydecontrol.practica2.model.DataAlbums
import kotlinx.android.synthetic.main.list_item_albums.view.*

class AlbumAdapter(private var albumList: List<DataAlbums>, private val context: Context)
        : RecyclerView.Adapter<AlbumAdapter.AlbumHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AlbumHolder =
        AlbumHolder(LayoutInflater.from(context).inflate(R.layout.list_item_albums, parent, false))

    override fun onBindViewHolder(holder: AlbumHolder, i: Int) {

        holder.bind(albumList[i])

        holder.itemView.setOnClickListener {

            val intent = Intent(context, AlbumDetails::class.java)
            intent.putExtra("name", holder.itemView.user_id.text.toString())

            context.startActivity(intent)
        }
    }

    override fun getItemCount() = albumList.size

    inner class AlbumHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(post: DataAlbums) {

            itemView.user_id.text = post.userId.toString()
            itemView.album_title.text = post.title
        }
    }
}

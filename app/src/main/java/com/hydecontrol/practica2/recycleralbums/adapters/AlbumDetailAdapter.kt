package com.hydecontrol.practica2.recycleralbums.adapters

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.hydecontrol.practica2.R
import com.hydecontrol.practica2.Utils.GlideApp
import com.hydecontrol.practica2.model.DataPhotos
import kotlinx.android.synthetic.main.list_item_photos.view.*

class AlbumDetailAdapter (private var albumDetailList: List<DataPhotos>, private val context: Context)
: RecyclerView.Adapter<AlbumDetailAdapter.AlbumDetailHolder>() {



    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AlbumDetailHolder =
        AlbumDetailHolder(LayoutInflater.from(context).inflate(R.layout.list_item_photos, parent, false))

    override fun onBindViewHolder(holder: AlbumDetailHolder, i: Int) {

        holder.bind(albumDetailList[i])

        GlideApp
            .with(context)
            .load(albumDetailList[i].url)
            .into(holder.itemView.photo_image)
            .getSize { width, height -> 50 }
    }

    override fun getItemCount() = albumDetailList.size

    inner class AlbumDetailHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(post: DataPhotos) {

            itemView.photo_text_details.text = post.albumId.toString()
        }
    }
}
package com.hydecontrol.practica2.model

data class DataComents(val name: String = "",
                       val postId: Int = 0,
                       val id: Int = 0,
                       val body: String = "",
                       val email: String = "")
package com.hydecontrol.practica2.model

import com.google.gson.annotations.SerializedName

data class DataAlbums(

        @SerializedName("userId")
        var userId: Int,
        @SerializedName("id")
        var id: Int,
        @SerializedName("title")
        val title: String)

package com.hydecontrol.practica2.model

data class DataPhotos
    (val albumId: Int = 0,
     val id: Int = 0,
     val title: String = "",
     var url: String = "",
     val thumbnailUrl: String = "")
package com.hydecontrol.practica2.model

data class Company(val bs: String = "",
                   val catchPhrase: String = "",
                   val name: String = "")
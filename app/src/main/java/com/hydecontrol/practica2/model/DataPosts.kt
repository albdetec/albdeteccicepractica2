package com.hydecontrol.practica2.model

import com.google.gson.annotations.SerializedName

data class DataPosts(

        @SerializedName("userId")
        var userId: Int,
        @SerializedName("id")
        var id: Int,
        @SerializedName("title")
        val title: String,
        @SerializedName("body")
        val body: String)

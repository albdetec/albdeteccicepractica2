package com.hydecontrol.practica2.model

data class DataUsers

    (val website: String = "",
    val address: Address,
    val phone: String = "",
    val name: String = "",
    val company: Company,
    val id: Int = 0,
    val email: String = "",
    val username: String = "")






package com.hydecontrol.practica2.model

import com.google.gson.annotations.SerializedName

class DataTotal (

    @SerializedName("userId")
    var userId: Int,
    @SerializedName("id")
    var id: Int,
    @SerializedName("title")
    val title: String,

    val albumId: Int = 0,
    val url: String = "",
    val thumbnailUrl: String = "")

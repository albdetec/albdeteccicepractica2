package com.hydecontrol.practica2.model

data class Geo(val lng: String = "",
               val lat: String = "")
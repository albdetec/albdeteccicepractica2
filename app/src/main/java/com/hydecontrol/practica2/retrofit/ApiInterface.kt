package com.hydecontrol.practica2.retrofit

import com.hydecontrol.practica2.model.*
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiInterface {


        @GET("posts")
        fun getPosts(): Call<List<DataPosts>>

        @GET("albums")
        fun getAlbums(): Call<List<DataAlbums>>

        @GET("users")
        fun getUsers(): Call<List<DataUsers>>

        @GET("photos?albumId=")
        fun getPhotos(@Query("albumId") albumId: String) : Call<List<DataPhotos>>

        @GET("comments?postId=")
        fun getComents(@Query("postId") postId: String) : Call<List<DataComents>>


}
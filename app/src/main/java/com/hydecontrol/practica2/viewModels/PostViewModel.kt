package com.hydecontrol.practica2.viewModels

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.content.IntentFilter
import android.support.v7.app.AlertDialog
import android.support.v7.widget.RecyclerView
import android.widget.Toast
import androidx.annotation.MainThread
import com.hydecontrol.practica2.R
import com.hydecontrol.practica2.R.id.post_input
import com.hydecontrol.practica2.model.DataAlbums
import com.hydecontrol.practica2.model.DataPosts
import com.hydecontrol.practica2.recyclerposts.activity.MainActivityPosts
import com.hydecontrol.practica2.recyclerposts.adapters.PostAdapter
import com.hydecontrol.practica2.retrofit.ApiClient
import io.reactivex.Completable
import kotlinx.android.synthetic.main.activity_main_posts.*
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class PostViewModel : ViewModel() {

    var originalPosts = ArrayList<DataPosts>()
    val oldFilteredPosts: MutableList<DataPosts> = mutableListOf()
    val filteredPosts: MutableList<DataPosts> = mutableListOf()



    fun search(query: String): Completable = Completable.create {

        val wanted = oldFilteredPosts.filter {
                it.userId.toString().equals(query)
            }.toList()

            filteredPosts.clear()
            filteredPosts.addAll(wanted)
            it.onComplete()
    }
}




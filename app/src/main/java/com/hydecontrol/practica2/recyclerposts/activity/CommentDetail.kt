package com.hydecontrol.practica2.recyclerposts.activity

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import com.hydecontrol.practica2.R
import com.hydecontrol.practica2.model.DataComents
import com.hydecontrol.practica2.recyclerposts.adapters.CommentDatailAdapter
import com.hydecontrol.practica2.retrofit.ApiClient
import kotlinx.android.synthetic.main.detail_comment.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class CommentDetail : AppCompatActivity() {


    var comentList = ArrayList<DataComents>()
    lateinit var recyclerView: RecyclerView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.detail_comment)
        recyclerView = recycler_comments

        recycler_comments.adapter = CommentDatailAdapter(comentList, this)
        recycler_comments.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)


        getData()
    }

    private fun getData() {

        val name = intent.getStringExtra("name")

        val call: Call<List<DataComents>> = ApiClient.getClient.getComents(name)
        call.enqueue(object : Callback<List<DataComents>> {

            override fun onResponse(call: Call<List<DataComents>>?, response: Response<List<DataComents>>?) {
                comentList.addAll(response!!.body()!!)
                recyclerView.adapter!!.notifyDataSetChanged()

                if (name != null) {
                post_detail_userid.text = comentList[1].postId.toString()
                post_detail_name.text = comentList[1].name
                post_detail_email.text = comentList[1].email}

            }
            override fun onFailure(call: Call<List<DataComents>>?, t: Throwable?) {
            }
        })
    }
}
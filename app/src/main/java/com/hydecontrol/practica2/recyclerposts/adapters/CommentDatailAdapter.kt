package com.hydecontrol.practica2.recyclerposts.adapters

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.hydecontrol.practica2.R
import com.hydecontrol.practica2.model.DataComents
import kotlinx.android.synthetic.main.list_item_comments.view.*

class CommentDatailAdapter (private var comentDetailList: List<DataComents>, private val context: Context)
    : RecyclerView.Adapter<CommentDatailAdapter.ComentDetailHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ComentDetailHolder =
        ComentDetailHolder(LayoutInflater.from(context).inflate(R.layout.list_item_comments, parent, false))

    override fun onBindViewHolder(holder: ComentDetailHolder, i: Int) {

        holder.bind(comentDetailList[i])

    }

    override fun getItemCount() = comentDetailList.size

    inner class ComentDetailHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(post: DataComents) {

            itemView.view_comments.text = post.body
            itemView.id_comments.text = post.postId.toString()
        }
    }
}
package com.hydecontrol.practica2.recyclerposts.activity

import android.arch.lifecycle.ViewModelProviders
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.util.DiffUtil
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.Gravity
import android.widget.Toast
import com.hydecontrol.practica2.R
import com.hydecontrol.practica2.model.DataPosts
import com.hydecontrol.practica2.recyclerposts.adapters.PostAdapter
import com.hydecontrol.practica2.Utils.PostsDiffUtilCallback
import com.hydecontrol.practica2.retrofit.ApiClient
import com.hydecontrol.practica2.viewModels.PostViewModel
import com.jakewharton.rxbinding2.widget.textChanges
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.activity_main_posts.*
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.concurrent.TimeUnit

class MainActivityPosts : AppCompatActivity() {

    private lateinit var viewModel: PostViewModel
    private val disposable = CompositeDisposable()
    lateinit var recyclerView: RecyclerView
    lateinit var toast:Toast

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main_posts)

        viewModel = ViewModelProviders.of(this).get(PostViewModel::class.java)

        recyclerView = recycler_View

        recyclerView.adapter = PostAdapter(viewModel.oldFilteredPosts, this)
        recyclerView.layoutManager = LinearLayoutManager(this)

        toast = Toast.makeText(this, "Elegiste Id", Toast.LENGTH_LONG)
        toast.setGravity(Gravity.TOP, 0, 0)


        post_input
            .textChanges()
            .debounce(200, TimeUnit.MILLISECONDS)
            .subscribe {

                viewModel.oldFilteredPosts.addAll(viewModel.originalPosts)

                viewModel
                    .search(post_input.text.toString())
                    .subscribeOn(Schedulers.computation())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe {
                        val diffResult = DiffUtil.calculateDiff(
                            PostsDiffUtilCallback(
                                viewModel.oldFilteredPosts,
                                viewModel.filteredPosts
                            )
                        )

                        viewModel.oldFilteredPosts.clear()

                        if (post_input.text.isBlank() || post_input.text.toString() == "0") {
                            viewModel.oldFilteredPosts.addAll(viewModel.originalPosts) }
                        else
                            toast.show()
                            viewModel.oldFilteredPosts.addAll(viewModel.filteredPosts)
                            diffResult.dispatchUpdatesTo(recycler_View.adapter!!)
                        }.addTo(disposable)
                    }.addTo(disposable)

        getData()
    }

    fun getData() {
        val call: Call<List<DataPosts>> = ApiClient.getClient.getPosts()
        call.enqueue(object : Callback<List<DataPosts>> {

            override fun onResponse(call: Call<List<DataPosts>>?, response: Response<List<DataPosts>>?) {
                viewModel.originalPosts.addAll(response!!.body()!!)
                viewModel.oldFilteredPosts.addAll(viewModel.originalPosts)
                recyclerView.adapter!!.notifyDataSetChanged()
            }

            override fun onFailure(call: Call<List<DataPosts>>?, t: Throwable?) {
            }
        })
    }

}

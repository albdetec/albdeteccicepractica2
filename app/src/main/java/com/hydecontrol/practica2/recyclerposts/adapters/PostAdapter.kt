package com.hydecontrol.practica2.recyclerposts.adapters

import android.content.Context
import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.hydecontrol.practica2.R
import com.hydecontrol.practica2.model.DataPosts
import com.hydecontrol.practica2.recyclerposts.activity.CommentDetail
import kotlinx.android.synthetic.main.list_item_posts.view.*

class PostAdapter(private var postList: List<DataPosts>, private val context: Context)
        : RecyclerView.Adapter<PostAdapter.PostHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PostHolder {
     return   PostHolder(LayoutInflater.from(context).inflate(R.layout.list_item_posts, parent, false))
    }

    override fun onBindViewHolder(holder: PostHolder, i: Int) {

        holder.bind(postList[i])

        holder.itemView.setOnClickListener {

            val intent = Intent(context, CommentDetail::class.java)
            intent.putExtra("name", holder.itemView.view_userid.text)

            context.startActivity(intent)
        }
    }
    override fun getItemCount() = postList.size

    inner class PostHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(post: DataPosts) {

            itemView.view_posts.text = post.body
            itemView.view_userid.text =post.userId.toString()
        }
    }
}
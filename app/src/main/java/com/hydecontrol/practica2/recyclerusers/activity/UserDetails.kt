package com.hydecontrol.practica2.recyclerusers.activity

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.hydecontrol.practica2.R
import kotlinx.android.synthetic.main.detail_user.*

class UserDetails : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.detail_user)

        readIntent()
    }

    private fun readIntent() {
        var name = intent.getStringExtra("name")
        var username = intent.getStringExtra("username")
        var phone = intent.getStringExtra("phone")
        var email = intent.getStringExtra("email")

        text_name.text = name
        text_username.text = username
        text_phone.text = phone
        text_email.text = email

    }
}

package com.hydecontrol.practica2.recyclerusers.adapters

import android.content.Context
import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.hydecontrol.practica2.R
import com.hydecontrol.practica2.recyclerusers.activity.UserDetails
import com.hydecontrol.practica2.model.DataUsers
import kotlinx.android.synthetic.main.list_item_users.view.*

class UserAdapter(private var userList: ArrayList<DataUsers>, private val context: Context)
        : RecyclerView.Adapter<UserAdapter.UsertHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): UsertHolder =
        UsertHolder(LayoutInflater.from(context).inflate(R.layout.list_item_users, parent, false))

    override fun onBindViewHolder(holder: UsertHolder, i: Int) {

        holder.bind(userList[i])
        var datos = userList[i]

        holder.itemView.setOnClickListener {

            val intent = Intent(context, UserDetails::class.java)
            intent.putExtra("name", holder.itemView.view_users.text.toString())
            intent.putExtra("username", datos.username)
            intent.putExtra("phone", datos.phone)
            intent.putExtra("email", datos.email)

            context.startActivity(intent)
        }
    }

    override fun getItemCount() = userList.size

    inner class UsertHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(post: DataUsers) {

            itemView.view_users.text = post.name
        }
    }
}
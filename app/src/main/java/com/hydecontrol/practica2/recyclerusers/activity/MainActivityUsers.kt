package com.hydecontrol.practica2.recyclerusers.activity

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.util.DiffUtil
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import com.hydecontrol.practica2.R
import com.hydecontrol.practica2.model.DataUsers
import com.hydecontrol.practica2.recyclerusers.adapters.UserAdapter
import com.hydecontrol.practica2.retrofit.ApiClient
import com.jakewharton.rxbinding2.widget.textChanges
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_main_users.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.concurrent.TimeUnit

class MainActivityUsers : AppCompatActivity() {

    var userList = ArrayList<DataUsers>()
    lateinit var recyclerView: RecyclerView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main_users)
        recyclerView = recycler_view_users

        //setting up the adapter
        recyclerView.adapter= UserAdapter(userList,this)
        recyclerView.layoutManager=LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false)

        getData3()
    }

    private fun getData3() {
        val call: Call<List<DataUsers>> = ApiClient.getClient.getUsers()
        call.enqueue(object : Callback<List<DataUsers>> {

            override fun onResponse(call: Call<List<DataUsers>>?, response: Response<List<DataUsers>>?) {
                userList.addAll(response!!.body()!!)
                recyclerView.adapter?.notifyDataSetChanged()
            }

            override fun onFailure(call: Call<List<DataUsers>>?, t: Throwable?) {
            }

        })
    }

}


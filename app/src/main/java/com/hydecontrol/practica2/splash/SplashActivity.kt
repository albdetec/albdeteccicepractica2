package com.hydecontrol.practica2.splash

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.os.Handler
import android.support.v7.app.AppCompatActivity
import android.view.WindowManager
import com.hydecontrol.practica2.main.MainActivity
import com.hydecontrol.practica2.tutorial.Pager
import com.hydecontrol.practica2.R
import com.hydecontrol.practica2.model.DataAlbums
import com.hydecontrol.practica2.model.DataPosts
import com.hydecontrol.practica2.model.DataUsers
import com.hydecontrol.practica2.retrofit.ApiClient
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class  SplashActivity : AppCompatActivity(){

        val PrefName = "prefname"
        var context = this

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN)
        setContentView(R.layout.activity_splash)

        var count = getSharedPreferences(PrefName, 0)
        var editor : SharedPreferences.Editor = (count as SharedPreferences).edit()
        var databack : SharedPreferences = getSharedPreferences(PrefName, 0)

            Handler().postDelayed({

                if (databack.getString("visto","segundavez") != "segundavez"){

                        startActivity(Intent(this, Pager::class.java))}

                else {startActivity(Intent(this, MainActivity::class.java))}

                finish()} , 4000)

        editor.putString("visto", "segundavez")
        editor.apply()

    }

}


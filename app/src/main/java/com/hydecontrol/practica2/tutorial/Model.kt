package com.hydecontrol.practica2.tutorial

import com.hydecontrol.practica2.R

enum class Model(val titleResId: Int, val layoutResId: Int) {
    POSTS(R.string.posts, R.layout.layout_tutorial_posts),
    ALBUMS(R.string.albums, R.layout.layout_tutorial_albums),
    USERS(R.string.users, R.layout.layout_tutorial_users)
}
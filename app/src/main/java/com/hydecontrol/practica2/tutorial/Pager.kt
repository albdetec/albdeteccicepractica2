package com.hydecontrol.practica2.tutorial

import android.content.Intent
import android.os.Bundle
import android.support.v4.view.ViewPager
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.Button
import com.hydecontrol.practica2.R
import com.hydecontrol.practica2.main.MainActivity
import kotlinx.android.synthetic.main.activity_pager.*


class Pager : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_pager)

        val button : Button = button_saltar

        button.setOnClickListener {startActivity(Intent(this, MainActivity::class.java))}

        val viewPager = findViewById<View>(R.id.viewpager) as ViewPager
        viewPager.adapter = PagerAdapter(this)


    }
}